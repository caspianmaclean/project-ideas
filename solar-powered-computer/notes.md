# Ideal

My ideal would be something like a Casio PB-100, but the screen can stay on powered by indoor lighting, like a solar powered calculator.

Also, it wouldn't need any batteries or electrolytic capacitors - many microcomputers from the 1980s have had their capacitors leaks after a few decades.

Also, it wouldn't use any power while waiting - the CPU could be off and the display static.

# Constraints

Need to work out how much power is needed for display, and how much power can be provided by the cells. You can probably run a processor at low enough power to use solar cells, and still be able to keep up with your typing, and run program slowly. So the display is the most challengin part to power.

# EEVblog video

In EEVblog #48 - Solar Power Hope, Dave from EEVblog looked at solar power for his calculator he designed, and found the power provided was too low. Link: https://www.youtube.com/watch?v=bdcLvrDdGTA

It would provide under 30 microamps at 3.3V - 30 microamps was the short-circuit current (I'm not sure how operating current would compare), and it would drop further when shaded.

He mentioned needing about 100 microamps for the display.

# Display options

One type of display I looked at was a 2.13 inch e-paper display from waveshare.

https://core-electronics.com.au/waveshare-2-13inch-e-paper-module-for-raspberry-pi-pico-250x122-black-white.html

But these have an update time of 0.3s to 2s, which would be annoyingly slow while typing, and also use 26mW while updating. If you typed more than 3 characters per second, they need to continuously use 26mW, which is higher power than many LCDs.

So I'd probably give up on not using any power in standby. It would be possible to have two displays, and transfer the display to the e-paper after it hasn't changed for a while. But that doesn't seem that useful.

Another type of display is 7 segment displays from solar calculators - while the power use would be low enough, I believe they need custom controllers, and also they 7 segment display would be very limiting, so they aren't suitable.

Another type again is Sharp memory displays. There's an example 1.3 inch one that says it uses 4 micro amps (about 12 micro Watts) https://core-electronics.com.au/adafruit-sharp-memory-display-breakout-1-3-168x144-monochrome.html This squarish shape isn't ideal, I'd prefer a longer one to display just one or two lines of text.
I've also seen 12 microWatts in the datasheet for the 96x96 1.3 inch sharp memory display (when updating every second)

There's a 2.7 inch 400x240 display, this takes more power.
Model: LS027B7DH01A

https://www.sharpsde.com/products/displays/model/ls027b7dh01a/

50 uW for static display refreshed at about 1 Hz.
175 uW when updated at 1 Hz.
(typical, there's a higher maximum listed)

And a 4.4 inch 320x240 display that uses even more, 100uW in hold mode, 600uW updating at 1 Hz (again, this is listed as typical, not maximum)

Another one that looks interesting is the LS021B7DD02, a 2.13 inch 320x240 colour display. Typical 32 uW for static, 45 uW for 1 Hz updates. This is for a white screen though, I don't know if that's lower power than when there's coloured and dark pixels.

There's a 1.17 inch 184X38 display I've seen at digikey, the LS012B7DD01, though it says it's not recommended for new designs.

I've found more here: https://www.sharpsde.com/products/displays/model/ls012b7dd01/

Based on its size it may take less power than the others, though it looks like it's designed to flip the LCD voltage polarity more often, so maybe it takes as much or more. It lists example power usage as 6, 45, 189 or 450 micro Watts, the higher ones are when updating the display. I'm not sure why updating the display takes much more power than on the larger displays.

# Lowering power to the display

With solar power calculators, when you lower the power available the display uses less power and fades to lower contrast. The Sharp memory display documentation doesn't encourage this, but maybe it's possible.

It may be that blank pixels or dark pixels take less power to display, and text (which is usually more blank pixels than not) may take less power than the vertical stripe display tested in the documentation.

You can update one or multiple lines at once - it may take much less power to update a few lines than the whole display, and 5 to 8 lines may be enough to update one or more characters typed in a row.

A 1 Hz update rate may be tolerable if there's some other feedback (such as sound) as soon as you press the key. Or maybe the computer can have a 1 Hz refresh rate for the display generally, but update and refresh it faster sometime when it's changing.

# Power from photovoltaic cells

Need to work out how much power we can get, which depends on the area.

Maybe around a 3x5 cm area could be dedicated to the cells, on a pocket computer sized device. Or the device could be made larger, like a Sinclair Z88, and a larger area could be dedicated to PV cells.

# Keyboard

It should be fairly straightforward to connect a keyboard matrix so that when a key is pressed (which closes a switch), it wakes or turns on the processor.

Once a key is pressed, while waiting for it to be released, maybe it conducts some current into a pulldown resistor. This drains power - see if it can be small enough to be insignificant, or maybe the keyboard should be sampled periodically, every few milliseconds, while the key is down, and unpowered in between samples. Of course the timer timing the sampling period should be low power.

If the processor starts from "off" each time a key is pressed, there may be more bootup time than when waking from sleep. Only do this if it still feels responsive, and the extra runtime doesn't take too much power.

# Memory

The computer could use at least a few kilobytes of FRAM, which doesn't need power when it's idle, and takes less power to modify than flash. This would be enough for matching an old pocket computer, but more could be useful.

# Potatop

There's a similar project called the potatop, though not pocket sized. Though it's actually been built.

https://hackaday.io/project/184340-potatop

It has almost a full sized keyboard, the keyboard matrix from a fairly normal keyboard but with new electronics connecting directly to the keyboard matrix.

Uses a 4.4 inch Sharp Memory LCD

It uses a SparkFun RedBoard Artemis ATP - boards based on the Sparkfun Artemis module look promising - low power ARM based microcontroller module, Arduino compatible, and ways of reducing the unneeded power used by the board (e.g. disabling the power LED).

Hasn't achieved pure indoor solar power yet, but very long battery life.

Uses lisp.

